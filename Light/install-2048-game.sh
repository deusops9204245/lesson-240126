#!/bin/bash

# Установка Nginx
sudo apt-get update
sudo apt-get install -y nginx

# Установка Node.js 16
sudo apt install apt-transport-https -y
sudo apt install lsb-release -y
sudo apt install curl -y
sudo apt install gnupg -y
sudo curl -sLf -o /dev/null 'https://deb.nodesource.com/node_16.x/dists/focal/Release'
sudo curl -s https://deb.nodesource.com/gpgkey/nodesource.gpg.key | gpg --dearmor | tee /usr/share/keyrings/nodesource.gpg >/dev/null
echo 'deb [signed-by=/usr/share/keyrings/nodesource.gpg] https://deb.nodesource.com/node_16.x focal main' > /etc/apt/sources.list.d/nodesource.list
echo 'deb-src [signed-by=/usr/share/keyrings/nodesource.gpg] https://deb.nodesource.com/node_16.x focal main' >> /etc/apt/sources.list.d/nodesource.list
apt-get update
curl -sL https://dl.yarnpkg.com/debian/pubkey.gpg | gpg --dearmor | sudo tee /usr/share/keyrings/yarnkey.gpg >/dev/null
sudo apt-get update && sudo apt-get install yarn
#sudo -E /bin/bash /opt/vagrant/setup_16.x.sh
sudo apt-get install -y nodejs

# Замена файла default в site-available для Nginx
sudo mv /etc/nginx/sites-available/default /etc/nginx/sites-available/default.bak
sudo cp /opt/vagrant/default /etc/nginx/sites-available/default

# Перезапуск Nginx
sudo systemctl restart nginx

# Клонирование репозитория 2048-game
git clone https://gitfront.io/r/deusops/JnacRhR4iD8q/2048-game.git

# Переход в папку скачанной программы
cd 2048-game

# Установка зависимостей и сборка
sudo npm install --include=dev
sudo npm run build

# Создание службы для команды npm start
sudo tee /etc/systemd/system/2048.service > /dev/null <<EOT
[Unit]
Description=2048 Game
After=network.target

[Service]
WorkingDirectory=/home/vagrant/2048-game
ExecStart=/usr/bin/npm start
Restart=always
User=root

[Install]
WantedBy=multi-user.target
EOT

# Перезагрузка службы
sudo systemctl daemon-reload

# Включение службы при старте системы
sudo systemctl enable 2048.service

# Запуск службы
sudo systemctl start 2048.service